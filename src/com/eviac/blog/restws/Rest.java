package com.eviac.blog.restws;

/**
 * REST SERVICE
 */
import java.net.UnknownHostException;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * GSON for deserialize JSON To Class
 */
import com.google.gson.Gson;


/**
 * MONGODB
 */
import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;


//@Path here defines class level path. Identifies the URI path that 
//a resource class will serve requests for.
@Path("")
public class Rest 
{

	// @PUT here defines, this method will method will process HTTP PUT requests.
	@PUT
	// @Path here defines method level path. Identifies the URI path that a
	// resource class method will serve requests for.
	@Path("/{nameGame}")
	
	@Consumes("application/json")
	// @PathParam injects the value of URI parameter that defined in @Path
	// expression, into the method.
	public void userName(@PathParam("nameGame") String nameGame, String infos) throws UnknownHostException 
	{
		InformationsPlayer json = new Gson().fromJson(infos,InformationsPlayer.class);
		
		// To directly connect to a single MongoDB server (note that this will not auto-discover the primary even
		// if it's a member of a replica set:
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		DB db = mongoClient.getDB( "mydb" );
		
		DBCollection coll = db.getCollection("shows");
		BasicDBObject doc = new BasicDBObject("badge", json.badge).
                append("nom", json.player).
                append("jeu", nameGame);

		coll.insert(doc);
	}

}
